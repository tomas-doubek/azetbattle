<?php

/*
 *
 *
 * ************ DATABASE CONNECT ********************************************************************************
 *
 */
define('DB_HOST', 'localhost');  // set database host
define('DB_USER', 'root');   // set database user
define('DB_PASS', '');                // set database password
define('DB_NAME', 'polls');   // set database name
define('VENDOR_ID', '1');   // set database name



/*
 *
 *
 * ************ APPLICATION SETTING ******************************************************************************
 *
 */
define('WWW_FOLDERS', "/azetbattle");



/*
 *
 *
 * ************DEFINITIONS OF SERVER PATH ************************************************************************
 *
 */
//URL PATH
define('HTTP_PROTOCOL', (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' || $_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://" ); //protocol
define('SERVER_NAME', $_SERVER['SERVER_NAME']); //server path too root file usualy index.php or home.php
define('WWW_PATH', HTTP_PROTOCOL . @$_SERVER['SERVER_NAME'] . WWW_FOLDERS . "/"); //server path too root file usualy index.php or home.php
//WEBHOOKS
define('WWW_WEBHOOKS', @$_SERVER['REQUEST_URI']); //webhooks
//FULL PATH
define('WWW_FULL_PATH', HTTP_PROTOCOL . @$_SERVER['SERVER_NAME'] . @$_SERVER['REQUEST_URI']); //full virtual path of actual address
define('STATIC_FILES_ADMIN', WWW_PATH . "public/backend/");
define('STATIC_FILES_WEB', WWW_PATH . "public/frontend/");

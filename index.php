<?php

/**
 * part:    @autoloader
 * author:  @Tomas Doubek
 */
include "class/Autoload.php";
$autoload = new Autoload;
$path = "";
$autoload->load($path);
$session = new Sessions();
$session->init();


/**
 * load current route
 */
$route = new Routes();
$route->loadRoute();

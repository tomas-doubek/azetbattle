<?php

/**
 *  class       PollsFrontend
 *  author      Tomas Doubek
 *  package     dnt3
 *  date        2017
 */
class PollsFrontend extends Polls {

    /**
     * 
     * @param type $index
     * @param type $poll_id
     * @param type $question_id
     * @return type
     */
    const RESULT = "result";

    public function url($index, $poll_id, $question_id) {
        $db = new Db;
        $rest = new Routes();

        $next_question = false;
        $prev_question = false;

        //first question 
        $query = "SELECT `question_id` FROM dnt_polls_composer WHERE
		vendor_id 	= " . Vendor::getId() . " AND
		`key`       = 'question' AND
		poll_id 	= '" . $poll_id . "' LIMIT 1";
        if ($db->num_rows($query) > 0) {
            foreach ($db->get_results($query) as $row) {
                $first_question = $row['question_id'];
            }
        } else {
            $first_question = false;
        }

        //next question
        $query = "SELECT `question_id` FROM dnt_polls_composer WHERE
		vendor_id 	= " . Vendor::getId() . " AND
		`key`       = 'question' AND
		`question_id` > '" . $question_id . "' AND
		poll_id 	= '" . $poll_id . "' LIMIT 1";
        if ($db->num_rows($query) > 0) {
            foreach ($db->get_results($query) as $row) {
                $next_question = $row['question_id'];
            }
        } else {
            //show result
            $next_question = self::RESULT;
        }

        //prev question
        $query = "SELECT `question_id` FROM dnt_polls_composer WHERE
		vendor_id 	= " . Vendor::getId() . " AND
		`key`       = 'question' AND
		`question_id` < '" . $question_id . "' AND
		poll_id 	= '" . $poll_id . "' LIMIT 1";
        if ($db->num_rows($query) > 0) {
            foreach ($db->get_results($query) as $row) {
                $prev_question = $row['question_id'];
            }
        } else {
            $prev_question = 1;
        }

        if ($index == "next") {
            if ($next_question == self::RESULT) {
                return WWW_PATH . "" . $rest->webhook(1) . "/" . self::RESULT . "/" . $poll_id . "/" . $rest->webhook(4) . "/";
            } else {
                return WWW_PATH . "" . $rest->webhook(1) . "/" . $rest->webhook(2) . "/" . $poll_id . "/" . $rest->webhook(4) . "/" . $next_question;
            }
        } elseif ($index == "prev") {
            return WWW_PATH . "" . $rest->webhook(1) . "/" . $rest->webhook(2) . "/" . $poll_id . "/" . $rest->webhook(4) . "/" . $prev_question;
        } elseif ($index == "first") {
            return WWW_PATH . "" . $rest->webhook(1) . "/" . $rest->webhook(2) . "/" . $poll_id . "/" . $rest->webhook(4) . "/" . $first_question;
        }
    }

    /**
     * 
     * @param type $poll_id
     * @param type $question_id
     * @return boolean
     */
    public function getCurrentQuestions($poll_id, $question_id) {
        $db = new Db;
        $query = "SELECT `value` FROM dnt_polls_composer WHERE
		vendor_id 	= " . Vendor::getId() . " AND
		poll_id 	= " . $poll_id . " AND
		question_id 	= " . $question_id . " AND
		`key`       = 'question'";
        if ($db->num_rows($query) > 0) {
            foreach ($db->get_results($query) as $row) {
                return $row['value'];
            }
        } else {
            return false;
        }
    }

    /**
     * 
     * @param type $poll_id
     * @return int
     */
    public function getPollsIds($poll_id) {
        $db = new Db;
        $arr = array();
        $query = "SELECT `question_id` FROM dnt_polls_composer WHERE
		vendor_id 	= " . Vendor::getId() . " AND
		`key`       = 'question' AND
		poll_id 	= '" . $poll_id . "'";
        if ($db->num_rows($query) > 0) {
            foreach ($db->get_results($query) as $row) {
                $arr[] = $row['question_id'];
            }
        } else {
            $arr[] = 0;
        }
        return $arr;
    }

    /**
     * 
     * @param type $vendor_ansewer_id
     * @return int
     * METODA 1
     */
    public function getCorrectOpinion($vendor_ansewer_id) {
        $db = new Db;
        $query = "SELECT `is_correct` FROM dnt_polls_composer WHERE
		vendor_id 	= " . Vendor::getId() . " AND
		is_correct 	= '1' AND
		id 	= '" . $vendor_ansewer_id . "'";
        if ($db->num_rows($query) > 0) {
            foreach ($db->get_results($query) as $row) {
                return $row['is_correct'];
            }
        } else {
            return 0;
        }
    }

    /**
     * 
     * @param type $poll_id
     * @return int
     * Funkcia vrati počet spravnych odpovedí v type ankety, kde je očakávaný počet percent ako výsledok
     */
    public function getCorrectAnsewers($poll_id) {
        $correct = 0;
        foreach (self::getPollsIds($poll_id) as $i) {
            $vendor_ansewer_id = Cookie::Get("poll_" . $poll_id . "_" . $i);
            self::getCorrectOpinion($vendor_ansewer_id);
            if (self::getCorrectOpinion($vendor_ansewer_id)) {
                $correct++;
            }
        }
        return $correct;
    }

    /**
     * 
     * @param type $poll_id
     * @return type
     * Funkcia vrati percentualny vysledok spravnych odpovedi
     */
    public function getResultPercent($poll_id) {
        return (100 * self::getCorrectAnsewers($poll_id)) / self::getNumberOfQuestions($poll_id);
    }

    /**
     * 
     * @param type $vendor_ansewer_id
     * @return int
     * METODA 2
     */
    public function getVendorAnsewerPoints($vendor_ansewer_id) {
        $db = new Db;
        $query = "SELECT `points` FROM dnt_polls_composer WHERE
		vendor_id 	= " . Vendor::getId() . " AND
		id 	= '" . $vendor_ansewer_id . "'";
        if ($db->num_rows($query) > 0) {
            foreach ($db->get_results($query) as $row) {
                return $row['points'];
            }
        } else {
            return 0;
        }
    }

    /**
     * TYP 2
     * @param type $poll_id
     * @return type
     */
    public function getVendorPoints($poll_id) {
        $correct = 0;
        foreach (self::getPollsIds($poll_id) as $i) {
            $vendor_ansewer_id = Cookie::Get("poll_" . $poll_id . "_" . $i);
            self::getVendorAnsewerPoints($vendor_ansewer_id);
            if (self::getVendorAnsewerPoints($vendor_ansewer_id)) {
                $correct += self::getVendorAnsewerPoints($vendor_ansewer_id);
            }
        }
        return $correct;
    }

    /**
     * TYP 2
     * @param type $poll_id
     * @return type
     */
    public function getVendorResultPointsRange($poll_id) {
        $db = new Db;
        $points = self::getVendorPoints($poll_id);
        $data = array(false);
        $points_MAX = 0;
        $points_MIN = 0;

        $query = self::getWinningCombinationData($poll_id);
        if ($db->num_rows($query) > 0) {
            foreach ($db->get_results($query) as $row) {
                //ziska maximum z rozsahu
                if ($row['points'] >= $points) {
                    $points_MAX = $row['points'];
                    break; //zabezpeči len nasledujucu hodnotu, nie tie dalsie 
                }
            }

            foreach ($db->get_results($query) as $row) {
                //ziska minimum z rozsah
                if ($row['points'] < $points) {
                    $points_MIN = $row['points'];
                }
            }
        }
        $data = array(
            "max" => $points_MAX,
            "min" => $points_MIN + 1 //+1 preto, aby sa dopočítal rozsah
        );
        return $data;
    }

    /**
     * TYP 2
     * @param type $poll_id
     * @return boolean
     */
    public function getVendorResultPointsCat($poll_id) {
        $db = new Db;
        $points_range = self::getVendorResultPointsRange($poll_id);
        $poins_max = $points_range['max'];
        //$points_range = self::getVendorResultPointsRange($poll_id);

        $query = "SELECT * FROM dnt_polls_composer WHERE 
		`poll_id` = '$poll_id' AND
		`points` = '$poins_max' AND
		`key` = 'winning_combination' AND
		`vendor_id` = '" . Vendor::getId() . "'";
        if ($db->num_rows($query) > 0) {
            foreach ($db->get_results($query) as $row) {
                $points = $row['id'];
            }
        } else {
            $points = false;
        }

        return $points;
    }

    /**
     * 
     * @param type $column
     * @param type $id
     * @return boolean
     */
    public function getComposerDataById($column, $id) {
        $db = new Db;
        $query = "SELECT `$column` FROM dnt_polls_composer WHERE 
		`id` = '$id' AND
		`vendor_id` = '" . Vendor::getId() . "'";
        if ($db->num_rows($query) > 0) {
            foreach ($db->get_results($query) as $row) {
                $return = $row[$column];
            }
        } else {
            $return = false;
        }
        return $return;
    }

    /**
     * 
     * @param type $poll_id
     */
    public function deleteCookies($poll_id) {
        foreach (PollsFrontend::getPollsIds($poll_id) as $i) {
            Cookie::Delete("poll_" . $poll_id . "_" . $i);
        }
    }

    /**
     * TYP 3
     */

    /**
     * 
     * @param type $poll_id
     * @param type $question_id
     * @return string
     */
    public function getProperties($poll_id, $question_id) {
        $db = new Db;
        $query = "SELECT * FROM dnt_polls_composer WHERE 
		`poll_id`       = '$poll_id' AND
		`question_id`   = '$question_id' AND
		`key` = 'ans' AND
		`vendor_id` = '" . Vendor::getId() . "'";
        if ($db->num_rows($query) > 0) {
            foreach ($db->get_results($query) as $row) {
                $properties[] = str_replace("-", "", Std::name_url($row['value']));
                ;
            }
        } else {
            $properties[] = array(false);
        }
        return $properties;
    }

    /**
     * 
     * @param type $poll_id
     * @return int
     */
    public function getVendorWrittenAnsewer($poll_id) {
        $correct = 0;
        foreach (self::getPollsIds($poll_id) as $question_id) {

            $vendor_ansewer = Cookie::Get("poll_" . $poll_id . "_" . $question_id);
            $vendor_ansewerToLightString = str_replace("-", "", Std::name_url($vendor_ansewer));
            //var_dump("Tvoja odpoved:                        " . $vendor_ansewer);
            //var_dump("Tvoja odpoved po uprave stringu:      " . $vendor_ansewerToLightString);
            //var_dump("A takto si mal odpovedať");
            //var_dump(self::getProperties($poll_id, $question_id));
           //var_dump(self::getProperties($poll_id, $question_id));

            if (in_array($vendor_ansewerToLightString, self::getProperties($poll_id, $question_id))) {
                $correct++;
                //var_dump($correct);
            }
            //var_dump();
        }

        //var_dump($correct);
        return $correct;
    }

    public function getResultPercentType3($poll_id) {
        return (100 * self::getVendorWrittenAnsewer($poll_id)) / self::getNumberOfQuestions($poll_id);
    }
    
    public function gerCurrentQuestion($question_id, $poll_id){
        $db = new Db;
        $i = 0;
        $query = "SELECT id FROM `dnt_polls_composer` WHERE
		`vendor_id` 	= " . Vendor::getId() . " AND
		`key`       = 'question' AND
		`question_id` <= '" . $question_id . "' AND
                `poll_id` 	= '" . $poll_id . "'";
        if ($db->num_rows($query) > 0) {
            foreach ($db->get_results($query) as $row) {
                 $i++;
            }
        }
        return $i;
    }
}

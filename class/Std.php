<?php

/**
 *  class       Std 
 *  author      Tomas Doubek
 *  package     dnt3
 *  date        2017
 */
class Std {

    public static function showStatus() {
        return array(
            "0" => "Vymazať",
            "1" => "Publikovať post",
            "2" => "Povoliť na webe (nezobrazí sa v menu alebo listingu)",
            "3" => "Skryť z webu",
        );
    }

    /**
     * 
     * @param type $file
     */
    public static function fileInclude($file) {
        if (file_exists($file)) {
            include $file;
        }
    }

    /**
     * 
     * @param type $pharse
     * @param type $str
     * @return type
     */
    public static function inString($pharse, $str) {
        return preg_match('/' . $pharse . '/', $str);
    }

    /**
     * 
     * @param type $presmeruj_url
     */
    public static function redirect($presmeruj_url) {
        if (!headers_sent()) {
            header('Location: ' . $presmeruj_url);
        } else {
            echo '<script type="text/javascript">';
            echo 'window.location.href="' . $presmeruj_url . '"';
            echo '</script>';
            echo '<meta http-equiv="refresh" content="0;url=' . $presmeruj_url . '" />';
        }
    }

    /**
     * 
     */
    public static function returnInput() {
        echo "<input type='hidden' name='return' value='" . WWW_FULL_PATH . "' />";
    }

    /**
     * 
     * @param type $msg
     * @return type
     */
    public static function confirmMsg($msg) {
        return " onclick=\"return confirm('$msg');\" ";
    }

    /**
     * 
     * @param type $url_adresa
     * @return type
     */
    public static function name_url($url_adresa) {
        # všetky znaky, ktoré v unicode nie sú písmená, čísla alebo podtržítka nahradíme pomlčkou
        $url_adresa = preg_replace('/[^\pL0-9_]+/u', '-', $url_adresa);
        # trimneme pomlčky
        $url_adresa = trim($url_adresa, '-');
        # urobíme translit diakritiky (č sa stane c, á sa stane a ...)
        $url_adresa = iconv('utf-8', 'ASCII//TRANSLIT', $url_adresa);
        # prevod na lowercase
        $url_adresa = strtolower($url_adresa);
        # vyhodíme všetko, čo nie je pomlčka, písmeno, číslo alebo podtržítko
        $url_adresa = preg_replace('/[^-a-z0-9_]+/', '', $url_adresa);

        return $url_adresa;
    }

    /**
     * 
     * @return type
     */
    public static function datetime() {
        return date("Y-m-d H:i:s");
    }

}

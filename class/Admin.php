<?php

class Admin {

    const TYPE = "admin";

    /**
     * 
     * @return boolean
     */
    public function loginProcess() {
        $rest = new Routes();
        $session = new Sessions();

        $session->set("logged", false);
        $session->set("session_id", false);

        $email = $rest->post("email");
        $pass = $rest->post("pass");

        $db = new Db;
        $query = "SELECT pass FROM dnt_users WHERE type = '" . self::TYPE . "' AND email = '" . $email . "' AND vendor_id = '" . Vendor::getId() . "'";
        if ($db->num_rows($query) > 0) {
            foreach ($db->get_results($query) as $row) {
                $db_pass = $row['pass'];
            }
            if ($db_pass == md5($pass)) {
                $session->set("logged", 1);
                $session->set("session_id", $email);
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    /**
     * 
     * @return boolean
     */
    public function isLogged() {
        $session = new Sessions();
        if ($session->get("logged") == 1) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * 
     */
    public function logout() {
        $session = new Sessions();
        $session->set("logged", false);
        $session->set("session_id", false);
    }

}

<?php

/**
 *  class       Autoload
 *  author      Tomas Doubek
 *  package     dnt3
 *  date        2017
 */
class Autoload {

    public function load($path) {
        /**
         * CONFIG
         */
        include $path . "conf.php";

        /**
         * CLASS
         */
        include $path . "class/Db.php";
        include $path . "class/Image.php";
        include $path . "class/Std.php";
        include $path . "class/Routes.php";
        include $path . "class/Vendor.php";
        include $path . "class/Session.php";
        include $path . "class/Cookie.php";
        include $path . "class/Polls.php";
        include $path . "class/PollsFrontend.php";
        include $path . "class/Upload.php";
        include $path . "class/DntUpload.php";
        include $path . "class/Admin.php";
    }

}

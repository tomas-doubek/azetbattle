<?php
/**
 *  class       Routes
 *  author      Tomas Doubek
 *  framework   DntLibrary
 *  package     dnt3
 *  date        2017
 */

class Routes {
    
    var $get; //variable of get result
    var $post; //variable of get post
    var $escape; //variable of get escape
	
    /**
     * 
     * @return type
     * route config => array of routes
     */
    public function myRoutes() {

    return array(
        
    /**
     * FRONTEND
     */
        "error404" =>
            array(
                "routeName"     => "frontendError",
                "route"         => "/error/404",
                "routeStatic"   => "/error/404",
                "controller"    => "controllerFrontend",
                "action"        => "errorAction",
                "tpl"           => "tpl/frontend/error/404.php",
            ),
            
        "frontendIndex" =>
            array(
                "routeName"     => "frontendIndex",
                "route"         => "/",
                "routeStatic"   => "/",
                "controller"    => "controllerFrontend",
                "action"        => "indexAction",
                "tpl"           => "tpl/frontend/polls/list.php",
            ),
            
        "pollsFrontendIndex2" =>
            array(
                "routeName"     => "pollsFrontendIndex2",
                "route"         => "/kvizy",
                "routeStatic"   => "/kvizy",
                "controller"    => "controllerPollsFrontend",
                "action"        => "indexAction",
                "tpl"           => "tpl/frontend/polls/list.php",
            ),
            
        "pollsFrontendStep" =>
            array(
                "routeName"     => "pollsFrontendStep",
                "route"         => "/kviz/step/{eny::pollsFrontendStep}",
                "routeStatic"   => "/kviz/step/",
                "controller"    => "controllerPollsFrontend",
                "action"        => "stepAction",
                "tpl"           => "tpl/frontend/polls/step.php",
            ),
            
        "pollsFrontendResult" =>
            array(
                "routeName"     => "pollsFrontendResult",
                "route"         => "/kviz/result/{eny::pollsFrontendResult}",
                "routeStatic"   => "/kviz/result/",
                "controller"    => "controllerPollsFrontend",
                "action"        => "resultAction",
                "tpl"           => "tpl/frontend/polls/result.php",
            ),
            
        "pollsFrontendShare" =>
            array(
                "routeName"     => "pollsFrontendShare",
                "route"         => "/kviz/share/{eny::pollsFrontendShare}",
                "routeStatic"   => "/kviz/share/",
                "controller"    => "controllerPollsFrontend",
                "action"        => "shareAction",
                "tpl"           => "tpl/frontend/polls/share.php",
            ),
            
        "frontendError" =>
            array(
                "routeName"     => "frontendError",
                "route"         => "/error",
                "routeStatic"   => "/error",
                "controller"    => "controllerFrontend",
                "action"        => "errorAction",
                "tpl"           => "tpl/frontend/error/404.php",
            ),
            
        
    /**
     * BACKEND HOMEPAGE
    */
        "backendIndex" =>
            array(
                "routeName"     => "backendIndex",
                "route"         => "/admin",
                "routeStatic"   => "/admin",
                "controller"    => "controllerBackend",
                "action"        => "indexAction",
                "tpl"           => "tpl/backend/home/index.php",
            ),
        
        "backendLogin" =>
            array(
                "routeName"     => "backendLogin",
                "route"         => "/admin/login",
                "routeStatic"   => "/admin/login",
                "controller"    => "controllerBackend",
                "action"        => "loginAction",
                "tpl"           => "tpl/backend/login/index.php",
            ),
        
        "loginAutorizeAction" =>
            array(
                "routeName"     => "loginAutorizeAction",
                "route"         => "/admin/login/autorize",
                "routeStatic"   => "/admin/login/autorize",
                "controller"    => "controllerBackend",
                "action"        => "loginAutorizeAction",
                "tpl"           => "tpl/backend/login/index.php",
            ),
        
        "logoutAction" =>
            array(
                "routeName"     => "logoutAction",
                "route"         => "/admin/logout",
                "routeStatic"   => "/admin/logout",
                "controller"    => "controllerBackend",
                "action"        => "logoutAction",
                "tpl"           => "tpl/backend/login/index.php",
            ),
            
    /**
    * BACKEND POLLS LIST
    */
        "pollsBackendIndex" =>
            array(
                "routeName"     => "pollsBackendIndex",
                "route"         => "/admin/polls/{eny::pollsBackendIndex}",
                "routeStatic"   => "/admin/polls",
                "controller"    => "controllerPollsBackend",
                "action"        => "indexAction",
                "tpl"           => "tpl/backend/polls/index.php",
            ),
        );
    }

    /**
     * 
     * @return type
     */
    public function getCurrentRoute() {
        $tmp = explode(WWW_FOLDERS, WWW_WEBHOOKS);
        return $tmp[1];
    }
    
    /**
     * 
     * @return type
     */
    public function webhooks() {
        return explode("/", $this->getCurrentRoute());
    }
    
    /**
     * 
     * @param type $i
     * @return type
     */
    public function webhook($i) {
        $webhooks = $this->webhooks();
        if(isset($webhooks[$i])){
            return $webhooks[$i];
        }
        return false;
    }


    /**
     * 
     * @return boolean
     */
    public function getCurrentRouteName() {
        $routes = $this->myRoutes();
        foreach ($routes as $route) {
            if ($route['route'] == $this->getCurrentRoute()) {
                return $route['routeName'];
            }elseif(Std::inString("{eny::".$route['routeName']."}", $route['route'])){
                $data = explode("{eny::".$route['routeName']."}", $route['route']);
                $WWW_WEBHOOKS       = str_replace("/", "-", WWW_WEBHOOKS);
                $WWW_ROUTESTATIC    = str_replace("/", "-", $route['routeStatic']);
                if(Std::inString($WWW_ROUTESTATIC, $WWW_WEBHOOKS)){
                    return $route['routeName'];
                }
            }
        }
        return false;
    }

    /**
     * 
     * @return boolean
     */
    public function loadRoute() {
        $routes = $this->myRoutes();
        if(isset($routes[$this->getCurrentRouteName()])){
            $route = $routes[$this->getCurrentRouteName()];
        }else{
            $route = $routes['frontendError'];
        }

        $controller = $route['controller'];
        $action     = $route['action'];

        $loadController = "controllers/" . $controller . ".php";
        if (file_exists($loadController)) {
            include $loadController;
            $data = new $controller;
            return $data->$action($route);
                     
        }
        return false;
    }
	
	
	/**
     * 
     * @param type $get
     * @return type
     * this method creat a GET method of `default` and `rewrited` addr
     */
    public function get($get) {

        @$addr1 = explode($get . "=", WWW_FULL_PATH);
        @$addr = $addr1[1];

        if (explode("&", @$addr1[1]) == true) {
            @$addr2 = explode("&", $addr1[1]);
            $this->get = $addr2[0];
        } else {
            $this->get = $addr;
        }
        return $this->get;
    }
   
    /**
     * 
     * @param type $post
     * @return type
     */
    public function post($post) {
        if (isset($_POST[$post])) {
            $this->post = @$_POST[$post];
        } else {
            $this->post = false;
        }

        return $this->post;
    }

}

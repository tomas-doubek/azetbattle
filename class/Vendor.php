<?php

/**
 *  class       Vendor
 *  author      Tomas Doubek
 *  package     dnt3
 *  date        2017
 */
class Vendor {

    /**
     * 
     * @return boolean
     */
    public static function getId() {
        $vendor_id = VENDOR_ID;
        return $vendor_id;
    }

}

Designdnt3 - Skeleton Aplication
=======================

Úvod
------------
- Jednoduchá kvízová platforma pre 3 typy kvízov

Technológia
------------
- Kvízová platforma je napísaná v jazyku PHP v plne objektovom orientovanom programovaní (OOP) a s použitím design patternu MVC. Aplikácia ponúka 3 typy kvízov.

Interface
------------
- jednoduchý prehladný dizajn
- kompletna administrácia
- možnost pridanie ďalšej otázky ku kvízu
- možnosť odstránenia otázky z kvízu
- editácia otázok
- pridávanie fotografii
- vytvaranie novych kvizov
- kopírovanie kvízov


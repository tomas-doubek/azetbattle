<?php

class controllerPollsFrontend {

    /**
     * 
     * @param type $route
     */
    public function indexAction($route) {
        Std::fileInclude("tpl/frontend/tpl_functions.php");
        Std::fileInclude($route["tpl"]);
    }

    /**
     * 
     * @param type $route
     */
    public function stepAction($route) {

        $rest = new Routes();
        $db = new DB();
        $poll_id = $rest->webhook(3);
        $question_id = $rest->webhook(5);

        if (in_array($question_id, PollsFrontend::getPollsIds($poll_id))) {
            Std::fileInclude("tpl/frontend/tpl_functions.php");
            Std::fileInclude($route["tpl"]);
        } else {
            Std::redirect(PollsFrontend::url("first", $poll_id, $question_id));
        }
    }

    /**
     * 
     * @param type $route
     */
    public function resultAction($route) {

        Std::fileInclude("tpl/frontend/tpl_functions.php");
        Std::fileInclude($route["tpl"]);
    }

    /**
     * 
     * @param type $route
     */
    public function shareAction($route) {

        Std::fileInclude("tpl/frontend/tpl_functions.php");
        Std::fileInclude($route["tpl"]);
    }

    /**
     * 
     * @param type $route
     */
    public function errorAction($route) {

        Std::fileInclude($route["tpl"]);
    }

}

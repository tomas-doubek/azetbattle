<?php

/**
 * default frontend controller
 */
class controllerFrontend {

    /**
     * 
     * @param type $route
     */
    public function indexAction($route) {
        Std::redirect(WWW_PATH . "kvizy");
    }

    /**
     * 
     * @param type $route
     */
    public function errorAction($route) {

        Std::fileInclude($route["tpl"]);
    }

}

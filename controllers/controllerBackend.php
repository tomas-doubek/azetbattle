<?php

/**
 * default frontend controller
 */
class controllerBackend {

    /**
     * 
     * @param type $route
     */
    public function indexAction($route) {
        if (Admin::isLogged()) {
            Std::fileInclude("tpl/backend/tpl_functions.php");
            Std::fileInclude($route["tpl"]);
        } else {
            Std::redirect(WWW_PATH . "admin/login");
        }
    }

    /**
     * 
     * @param type $route
     */
    public function loginAction($route) {
        Std::fileInclude("tpl/backend/tpl_functions.php");
        Std::fileInclude($route["tpl"]);
    }

    /**
     * 
     * @param type $route
     */
    public function loginAutorizeAction($route) {
        Admin::loginProcess();
        Std::redirect(WWW_PATH . "admin");
    }

    /**
     * 
     * @param type $route
     */
    public function logoutAction($route) {
        Admin::logout();
        Std::redirect(WWW_PATH . "admin");
    }

}

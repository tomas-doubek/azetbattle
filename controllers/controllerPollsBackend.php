<?php

class controllerPollsBackend {

    /**
     * 
     * @param type $route
     */
    public function indexAction($route) {
        $rest = new Routes();
        $action = $rest->get('action');
        $post_id = $rest->get('post_id');

        if ($action == "edit_poll") {
            $post_id = $rest->get('post_id');
            Std::fileInclude("tpl/backend/tpl_functions.php");
            Std::fileInclude("tpl/backend/polls/edit.php");
            
        } elseif ($action == "edit_poll_action") {
            Polls::updatePollData();
            
        } elseif ($action == "add_poll") {
            Std::fileInclude("tpl/backend/tpl_functions.php");
            Std::fileInclude("tpl/backend/polls/add.php");
           
        } elseif ($action == "del_question") {
            Polls::delQuestion($rest->get("post_id"), $rest->get("question_id"));
            Std::redirect("?action=edit_poll&post_id=".$rest->get("post_id"));
        
            
        } elseif ($action == "add_winning_combination") {
            Polls::AddWinningCombination($rest->get("post_id"), $rest->get("question_id"));
            Std::redirect("?action=edit_poll&post_id=".$rest->get("post_id"));
            
        } elseif ($action == "del_winning_combination") {
            Polls::delComposerInput($rest->get("composer_id"));
            Std::redirect("?action=edit_poll&post_id=".$rest->get("post_id"));
        } elseif ($action == "add_question") {
            Polls::addQuestion($rest->get("post_id"), $rest->get("question_id"));
            Std::redirect("?action=edit_poll&post_id=".$rest->get("post_id"));
            
        }elseif ($action == "add_poll_action") {
            Polls::addPoll();
            
        }else {
            Std::fileInclude("tpl/backend/tpl_functions.php");
            Std::fileInclude($route["tpl"]);
        }
    }

}

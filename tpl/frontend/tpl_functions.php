<?php

function get_top() { ?>
    <!DOCTYPE html>
    <!--[if IE 9]> <html lang="sk" class="ie9"> <![endif]-->
    <!--[if !IE]><!--> 
    <html lang="sk"> <!--<![endif]-->
        <head>
            <meta charset="utf-8">
            <title>Skeletón</title>


            <!-- Meta -->
            <meta charset="utf-8">
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <meta name="description" content="">
            <meta name="author" content="">
            <meta name="keywords" content="skeleton, dnt3, designdnt, application" /><meta name="description" content="Designdnt corporation" /><meta content="" property="og:title" /><meta content="skeleton.localhost" property="og:site_name" /><meta content="article" property="og:type" /><meta content="" property="og:image" />	
            <!-- Favicon -->
            <link rel="shortcut icon" href="favicon.ico">
            <link rel="apple-touch-icon" sizes="57x57" href="<?php echo STATIC_FILES_WEB ?>img/favicon/apple-icon-57x57.png">
            <link rel="apple-touch-icon" sizes="60x60" href="<?php echo STATIC_FILES_WEB ?>img/favicon/apple-icon-60x60.png">
            <link rel="apple-touch-icon" sizes="72x72" href="<?php echo STATIC_FILES_WEB ?>img/favicon/apple-icon-72x72.png">
            <link rel="apple-touch-icon" sizes="76x76" href="<?php echo STATIC_FILES_WEB ?>img/favicon/apple-icon-76x76.png">
            <link rel="apple-touch-icon" sizes="114x114" href="<?php echo STATIC_FILES_WEB ?>img/favicon/apple-icon-114x114.png">
            <link rel="apple-touch-icon" sizes="120x120" href="<?php echo STATIC_FILES_WEB ?>img/favicon/apple-icon-120x120.png">
            <link rel="apple-touch-icon" sizes="144x144" href="<?php echo STATIC_FILES_WEB ?>img/favicon/apple-icon-144x144.png">
            <link rel="apple-touch-icon" sizes="152x152" href="<?php echo STATIC_FILES_WEB ?>img/favicon/apple-icon-152x152.png">
            <link rel="apple-touch-icon" sizes="180x180" href="<?php echo STATIC_FILES_WEB ?>img/favicon/apple-icon-180x180.png">
            <link rel="icon" type="image/png" sizes="192x192"  href="<?php echo STATIC_FILES_WEB ?>img/favicon/android-icon-192x192.png">
            <link rel="icon" type="image/png" sizes="32x32" href="<?php echo STATIC_FILES_WEB ?>img/favicon/favicon-32x32.png">
            <link rel="icon" type="image/png" sizes="96x96" href="<?php echo STATIC_FILES_WEB ?>img/favicon/favicon-96x96.png">
            <link rel="icon" type="image/png" sizes="16x16" href="<?php echo STATIC_FILES_WEB ?>img/favicon/favicon-16x16.png">
            <link rel="manifest" href="<?php echo STATIC_FILES_WEB ?>img/favicon/manifest.json">
            <meta name="msapplication-TileColor" content="#ffffff">
            <meta name="msapplication-TileImage" content="<?php echo STATIC_FILES_WEB ?>img/favicon/ms-icon-144x144.png">
            <meta name="theme-color" content="#ffffff">




            <!-- Web Fonts -->
            <link rel='stylesheet' type='text/css' href='http://fonts.googleapis.com/css?family=Roboto+Slab:400,300,700'>

            <!-- CSS Global Compulsory -->
            <link rel="stylesheet" href="<?php echo STATIC_FILES_WEB ?>css/bootstrap.min.css">
            <link rel="stylesheet" href="<?php echo STATIC_FILES_WEB ?>css/blog.style.css">

            <!-- CSS Header and Footer -->
            <link rel="stylesheet" href="<?php echo STATIC_FILES_WEB ?>css/header-v8.css?241">
            <link rel="stylesheet" href="<?php echo STATIC_FILES_WEB ?>css/footer-v8.css">

            <!-- CSS Implementing Plugins -->
            <link rel="stylesheet" href="<?php echo STATIC_FILES_WEB ?>css/animate.css">
            <link rel="stylesheet" href="<?php echo STATIC_FILES_WEB ?>css/line-icons.css">
            <link rel="stylesheet" href="<?php echo STATIC_FILES_WEB ?>css/jquery.fancybox.css">
            <link rel="stylesheet" href="<?php echo STATIC_FILES_WEB ?>css/font-awesome.min.css">
            <link rel="stylesheet" href="<?php echo STATIC_FILES_WEB ?>css/style.css">
            <link rel="stylesheet" href="<?php echo STATIC_FILES_WEB ?>css/owl.carousel.css">
            <link rel="stylesheet" href="<?php echo STATIC_FILES_WEB ?>css/masterslider.css">
            <link rel="stylesheet" href="<?php echo STATIC_FILES_WEB ?>css/default.style.css">

            <!-- CSS Theme -->
            <link rel="stylesheet" href="<?php echo STATIC_FILES_WEB ?>css/default.colors.css" id="style_color">
            <link rel="stylesheet" href="<?php echo STATIC_FILES_WEB ?>css/dark.css">

            <!-- CSS Customization -->
            <link rel="stylesheet" href="<?php echo STATIC_FILES_WEB ?>css/custom.css?542">
            <link rel="stylesheet" href="<?php echo STATIC_FILES_WEB ?>css/green.css">
            
            <link rel="stylesheet" href="<?php echo STATIC_FILES_WEB ?>css/polls.css">
            <script src="<?php echo STATIC_FILES_WEB ?>js/jquery.min.js"></script>
            <script src="<?php echo STATIC_FILES_WEB ?>js/jquery.validate.js"></script>
        </head>
        <body data-spy="scroll" data-target=".navbar" class="header-fixed header-fixed-space-v2 pages-id">
            <div class="wrapper">
                <!--=== Header v8 ===-->
                <div class="header-v8 header-sticky">
                    <div class="blog-topbar">
                        <div class="topbar-search-block">
                            <div class="container">
                                <form action="">
                                    <input type="text" class="form-control" placeholder="Hľadať">
                                    <div class="search-close"><i class="fa fa-times" aria-hidden="true"></i></div>
                                </form>
                            </div>
                        </div>
                        <div class="container">
                            <div class="row">
                                <div class="col-sm-8 col-xs-8">


                                </div>
                                <div class="col-sm-4 col-xs-4 clearfix">

                                    <ul class="topbar-list topbar-log_reg pull-right visible-sm-block visible-md-block visible-lg-block">
                                        <li class="cd-log_reg home"><a class="cd-signin" href="javascript:void(0);"></a></li>
                                    </ul>
                                </div>
                            </div><!--/end row-->
                        </div><!--/end container-->
                    </div>
                    <!-- End Topbar blog -->

                    <!-- Navbar -->
                    <div class="navbar mega-menu" role="navigation">
                        <div class="container">
                            <!-- Brand and toggle get grouped for better mobile display -->
                            <div class="res-container">
                                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-responsive-collapse">
                                    <span class="sr-only">Toggle navigation</span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>
                                <div class="navbar-brand">
                                    <a href="<?php echo WWW_PATH; ?>admin">
                                        <img class="logo" src="http://skeleton.localhost/dnt3/dnt-view/data/1/logo-1.png" alt="Logo">
                                    </a>
                                </div>
                            </div>
                            <!--/end responsive container-->
                            <!-- Collect the nav links, forms, and other content for toggling -->
                            <div class="collapse navbar-collapse navbar-responsive-collapse">
                                <div class="res-container">
                                    <ul class="nav navbar-nav">
                                        <!-- Home -->
                                        <li class="dropdown home  ">
                                            <a  href="<?php echo WWW_PATH; ?>">Domov</a>
                                        </li>
                                        <li class="dropdown home  ">
                                            <a  href="<?php echo WWW_PATH; ?>kvizy">Kvízy</a>
                                        </li>
                                        <li class="dropdown admin  ">
                                            <a target="_blank" href="<?php echo WWW_PATH; ?>admin">Admin</a>
                                        </li>
                                    </ul>
                                </div>
                                <!--/responsive container-->
                            </div>
                            <!--/navbar-collapse-->
                        </div>
                        <!--/end contaoner-->
                    </div>
                    <!-- End Navbar -->
                </div>
<?php } ?>
<?php

function get_bottom() { ?>
                <!--=== Footer v8 ===-->
                <div class="footer-v8">
                    <footer class="footer">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-9 col-sm-6 column-one md-margin-bottom-50">
                                    <h2>Kontakt</h2>
                                    <div class="row">
                                        <div class="col-md-3">
                                            <span>Mesto:</span>
                                            <p>Bratislava</p>
                                            <hr/>
                                        </div>
                                        <div class="col-md-3">
                                            <span>Telefónne číslo:</span>
                                            <p>+421904700823</p>
                                            <hr/>
                                        </div>
                                        <div class="col-md-3">
                                            <span>Email:</span>
                                            <a href="mailto:thomas.doubek@gmail.com">thomas.doubek@gmail.com</a>
                                            <hr/>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-6">
                                    <h2>Sociálne siete</h2>
                                    <!-- Social Icons -->
                                    <ul class="social-icon-list margin-bottom-20">
                                        <li><a target="_blank" href="https://www.facebook.com/dynatom"><i class="rounded-x fa fa-facebook"></i></a></li>
                                        <li><a target="_blank" href="https://www.linkedin.com/in/tom%C3%A1%C5%A1-doubek-75a371b2/"><i class="rounded-x fa fa-linkedin"></i></a></li>
                                    </ul>
                                    <!-- End Social Icons -->
                                </div>
                            </div>
                            <!--/end row-->
                        </div>
                        <!--/end container-->
                    </footer>
                    <footer class="copyright">
                        <div class="container">
                            <ul class="list-inline terms-menu">
                                <li>2017 &copy; All Rights Reserved.</li>
                                <li class="home">Vytvorené ako aplikácia</li>
                                 <li><a href="http://www.azetbattle.sk/" target="_blank">Azetbattle</a></li>
                            </ul>
                        </div>
                        <!--/end container-->
                    </footer>
                </div>
                <!--=== End Footer v8 ===-->
                <!-- JS Global Compulsory -->
                <script src="<?php echo STATIC_FILES_WEB ?>js/jquery-migrate.min.js"></script>
                <script src="<?php echo STATIC_FILES_WEB ?>js/bootstrap.min.js"></script>


                <!-- JS Implementing Plugins -->
                <script src="<?php echo STATIC_FILES_WEB ?>js/back-to-top.js"></script>
                <script src="<?php echo STATIC_FILES_WEB ?>js/smoothScroll.js"></script>
                <script src="<?php echo STATIC_FILES_WEB ?>js/waypoints.min.js"></script>
                <script src="<?php echo STATIC_FILES_WEB ?>js/jquery.counterup.min.js"></script>
                <script src="<?php echo STATIC_FILES_WEB ?>js/jquery.fancybox.pack.js"></script>
                <script src="<?php echo STATIC_FILES_WEB ?>js/owl.carousel.js"></script>
                <script src="<?php echo STATIC_FILES_WEB ?>js/masterslider.js"></script>
                <script src="<?php echo STATIC_FILES_WEB ?>js/jquery.easing.min.js"></script>
                <script src="<?php echo STATIC_FILES_WEB ?>js/modernizr.js"></script>
                <script src="<?php echo STATIC_FILES_WEB ?>js/main.js"></script> <!-- Gem jQuery -->
                <script src="<?php echo STATIC_FILES_WEB ?>js/cookies.js"></script> <!-- Gem jQuery -->

                <!-- JS Customization -->
                <script src="<?php echo STATIC_FILES_WEB ?>js/custom.js"></script>
                <!-- JS Page Level -->


                <script src="<?php echo STATIC_FILES_WEB ?>js/app.js"></script>
                <script src="<?php echo STATIC_FILES_WEB ?>js/fancy-box.js"></script>
                <script src="<?php echo STATIC_FILES_WEB ?>js/owl-carousel.js"></script>
                <script src="<?php echo STATIC_FILES_WEB ?>js/master-slider-showcase1.js"></script>
                <script src="<?php echo STATIC_FILES_WEB ?>js/style-switcher.js"></script>

                <script type="text/javascript">
                    jQuery(document).ready(function () {
                        App.init();
                        App.initCounter();
                        FancyBox.initFancybox();
                        OwlCarousel.initOwlCarousel();
                        OwlCarousel.initOwlCarousel2();
                        StyleSwitcher.initStyleSwitcher();
                        MasterSliderShowcase1.initMasterSliderShowcase1();
                    });
                </script>
                <!--[if lt IE 9]>
                        <script src="assets/plugins/respond.js"></script>
                        <script src="assets/plugins/html5shiv.js"></script>
                        <script src="assets/plugins/placeholder-IE-fixes.js"></script>
                <![endif]-->
        </body>
    </html>

<?php } ?>

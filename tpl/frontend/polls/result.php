<?php
$rest = new Routes();
$db = new Db;
$poll_id = $rest->webhook(3);
$question_id = $rest->webhook(4);
$poll_input_name = "poll_" . $poll_id . "_" . $question_id;

get_top();
echo '<div style="margin-top: 60px;"></div>
<div class="container panel panel-primary dnt-poll">';

if (Polls::getParam("type", $poll_id) == 3) {
    $rst = round(PollsFrontend::getResultPercentType3($poll_id), 2);
    $img = Image::getPostImage($poll_id, "dnt_polls");
    $url = false;
    $title = Polls::getParam("name", $poll_id);
    $description = Polls::getParam("content", $poll_id);
    $shareUrl = '<a target="_blank" href="http://www.facebook.com/sharer/sharer.php?u=' . WWW_FULL_PATH . '?share=' . $rst . '&points=' . $rst . '>';
    $resultType = "percent";
} elseif (Polls::getParam("type", $poll_id) == 1) {
    $rst = round(PollsFrontend::getResultPercent($poll_id), 2);
    $img = Image::getPostImage($poll_id, "dnt_polls");
    $url = false;
    $title = Polls::getParam("name", $poll_id);
    $description = Polls::getParam("content", $poll_id);
    $shareUrl = '<a target="_blank" href="http://www.facebook.com/sharer/sharer.php?u=' . WWW_FULL_PATH . '?share=' . $rst . '&points=' . $rst . '>';
    $resultType = "percent";
} elseif (Polls::getParam("type", $poll_id) == 2) {
    $rst = PollsFrontend::getVendorPoints($poll_id); //returns yout points score
    $poll_range = PollsFrontend::getVendorResultPointsRange($poll_id); //return max of range
    $poll_max = $poll_range['max'];
    $poll_min = $poll_range['min'];
    $catId = PollsFrontend::getVendorResultPointsCat($poll_id); //return category ID witch you include
    $url = false;
    $description = PollsFrontend::getComposerDataById('description', $catId);
    $img = Image::getPostImage($catId, "dnt_polls_composer");
    $title = PollsFrontend::getComposerDataById('value', $catId);
    $shareUrl = '<a target="_blank" href="http://www.facebook.com/sharer/sharer.php?u=' . WWW_FULL_PATH . '?share=' . $catId . '&points=' . $rst . '>';
    $resultType = "points";
}
?>
<div class="container">
    <div class="row">
        <div class="col-md-1"></div>

        <div class="col-md-10">
            <div class="row">
                <?php 
                if($resultType== "percent"){
                    $string = "Gratulujeme, nas kviz ste zabsolvovali na  <span style='color:red'> ".$rst."%</span>";
                }elseif($resultType == "points"){
                    $string = "Gratulujeme, úspešne ste prešli naším kvízom  s počtom bodov <span style='color:red'>".$rst."</span>";
                }
                    
                ?>
                <h2><?php echo $string; ?></h2>
                <div style="margin-top: 60px;"></div>
                <hr/>
            </div>
            <div class="row">
                <div class="col-sm-4"><a href="<?php echo $url; ?>" class=""><img src="<?php echo $img; ?>" class="img-responsive"></a>
                </div>
                <div class="col-sm-8">
                    <h3 class="title"><a href="<?php echo $url; ?>"><?php echo $title; ?></a></h3>
                    <p class="text-muted"><span class="glyphicon glyphicon-lock"></span> 
                        Gratulujeme, úspešne ste prešli naším kvízom.
                    </p>
                    <p><?php echo $description; ?></p>
                    <p class="text-muted">powered by <a href="http://designdnt.query.sk/" target="_blank">designdnt</a></p>
                </div>
            </div>
            <div class="row">
                <h5><?php echo $shareUrl ?>Zdielať výsledok</a>'; ?></h5>
                <div style="margin-bottom: 60px;"></div>
                <hr/>
            </div>
        </div>
        <div class="col-md-1"></div>
    </div>
</div>


<?php /* echo "<h3> Výsledok: / Patríte do kategórie / {<span style='font-size:12px'>id kategorie na základe ktorej sa vyberú príslušné dáta </span> <span style='color:red'>".$catId."</span>} s počtom bodov <span style='color:red'>".$rst."</span></h3>";
  echo "<h3> Výsledok: / {<span style='font-size:12px'>bodovy rozsah kategorie je </span> <span style='color:red'>".$poll_min." - ".$poll_max."</span>}</h3>";

  echo '<a href="'.WWW_FULL_PATH.'?share='.$catId.'">SHARE URL</a>';
  echo '<img src="'.Image::getPostImage($catId,"dnt_polls_composer").'" style="height: 200px" />';
 */ ?>
<?php
echo '</div><div class="margin-bottom-60"></div>';
?>
<?php get_bottom(); ?>
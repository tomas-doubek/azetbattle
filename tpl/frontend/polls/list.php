<?php
get_top();
$rest = new Routes();
$db = new Db;
?>
<div class="blog-ms-v1 content-sm bg-color-darker margin-bottom-60">
    <div class="master-slider ms-skin-default" id="masterslider">
        <?php
        $query = Polls::getPolls();
        if ($db->num_rows($query) > 0) {
            foreach ($db->get_results($query) as $row) {
                $url = WWW_PATH . "kviz/step/" . $row['id'] . "/" . $row['name_url'] . "";
                ?>
                <div class="ms-slide blog-slider">
                    <img src="<?php echo Image::getPostImage($row['id'], "dnt_polls"); ?>" data-src="<?php echo Image::getPostImage($row['id'], "dnt_polls"); ?>" alt="<?php echo $row['name']; ?>"/>
                    <span class="blog-slider-badge" onclick="location.href = '<?php echo $url; ?>';"><?php echo Polls::currentTypeStr($row['type']); ?></span>
                    <div class="ms-info"></div>
                    <div class="blog-slider-title">
                        <a href="<?php echo $url; ?>">
                            <h2><?php echo $row['name']; ?></h2>
                        </a>
                    </div>
                </div>
                <?php
            }
        }
        ?>
    </div>
</div>
<div style="margin-top: 60px;"></div>
<div class="container panel panel-primary dnt-poll">
    <div class="col-md-12 homepage">
        <div class="masonry-box homepage-items">
            <div class="row">

                <div class="blog-grid masonry-box-in col-3">
                    <h3><a href="http-bmet-sk">Úvod<br><br> </a></h3>
                    <hr>
                    <p></p>Jednoduchá kvízová platforma pre 3 typy kvízov<p>

                    </p>
                    <p></p>
                    <a class="r-more" href="#">Čítať viac</a>
                </div>

                <div class="blog-grid masonry-box-in col-3">
                    <h3><a href="http-nastrojaren-osmos-sk">Technológia<br>&nbsp;</a></h3>
                    <hr>
                    <p></p>
                    <p>Kvízová platforma je napísaná v jazyku PHP v plne objektovom orientovanom programovaní (OOP) a s použitím design patternu MVC. 
                        Aplikácia ponúka 3 typy kvízov: <b><br/>Test <br/>Priraďovací kvíz <br/>Vpisovací kvíz</b></p>
                    <p></p>
                    <a class="r-more" href="#">Čítať viac</a>
                </div>

                <div class="blog-grid masonry-box-in col-3">
                    <h3><a href="vyskum-a-vyvoj">Interface<br>&nbsp;</a></h3>
                    <hr>
                    <p></p>

                        jednoduchý prehladný dizajn
                        <br/>kompletna administrácia
                        <br/>možnost pridanie ďalšej otázky ku kvízu
                        <br/>možnosť odstránenia otázky z kvízu
                        <br/>editácia otázok
                        <br/>pridávanie fotografii
                        <br/>vytvaranie novych kvizov
                        <br/>kopírovanie kvízov
                    <p></p>
                    <a class="r-more" href="#">Čítať viac</a>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="margin-bottom-60"></div>
<?php get_bottom(); ?>
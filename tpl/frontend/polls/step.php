<?php
$rest = new Routes;
$db = new Db;
$poll_id = $rest->webhook(3);
$question_id = $rest->webhook(5);
$poll_input_name = "poll_" . $poll_id . "_" . $question_id;
get_top();

$pollImg = Image::getPostImage($poll_id, "dnt_polls");
$currentQuestion = PollsFrontend::gerCurrentQuestion($question_id, $poll_id);
$countQuestion = PollsFrontend::getNumberOfQuestions($poll_id);
$percent = ($currentQuestion / $countQuestion) * 100;
?>


<div class="container">
    <section class="col-xs-12 col-md-8 no-padding polls_cont">



        <!-- core -->
        <div class="col-xs-12 tests no-padding">
            <h3 class="no-padding">
                <a href="<?php echo WWW_PATH; ?>"><i class="fa fa-bars" aria-hidden="true"></i></a> 
                <a href="<?php echo WWW_PATH; ?>">Zoznam kvízov</a>
            </h3>
        </div>
        <div class="col-xs-12 no-padding title">
            <!-- core -->
            <h2><?php echo Polls::getParam("name", $poll_id); ?></h2>
        </div>
        <div class="col-xs-12 no-padding">
            <p class="name">Otázka <?php echo $currentQuestion; ?> z <?php echo $countQuestion; ?> <small>(<?php echo $percent; ?>%)</small>:</p>
            <div class="progress">
                <div class="progress-bar" role="progressbar" aria-valuenow="10" aria-valuemin="0" aria-valuemax="<?php echo $percent; ?>" style="width:<?php echo $percent; ?>%"></div>
            </div>
        </div>
        <div class="col-xs-12 no-padding body">
            <!-- core -->
            <div class="question">

                <div class="col-xs-12 cover">
                    <img src="<?php echo $pollImg; ?>" class="img-responsive" style="max-width:300px">
                </div>
                <div class="col-xs-12 title">
                    <h3 class="col-xs-12 no-padding"><?php echo PollsFrontend::getCurrentQuestions($poll_id, $question_id); ?></h3>
                </div>
                <div class="col-xs-12 content">


                    <?php if (Polls::getParam("type", $poll_id) == 3) { ?>
                        <li class="list-group-item">
                            <div class="radio">
                                <label>
                                    <input type="text" class="form-control" name="<?php echo $poll_input_name; ?>" value="">
                                </label>
                            </div>
                        </li>
                        <?php
                    } else {
                        $query = Polls::getQuestions($poll_id, $question_id);
                        if ($db->num_rows($query) > 0) {
                            foreach ($db->get_results($query) as $row) {
                                ?>

                                <div class="input-wrap">
                                    <input id="<?php echo $row['id'] ?>" type="radio" name="<?php echo $poll_input_name; ?>" required="" value="<?php echo $row['id'] ?>">
                                    <label for="<?php echo $row['id'] ?>"><?php echo $row['value'] ?></label>
                                    <div class="check"><div class="inside"></div></div>
                                </div>


                                <?php
                            }
                        }
                    }
                    ?>



                </div>


                <div class="col-xs-12 no-padding">
                    <a href="aaa<?php echo PollsFrontend::url("prev", $poll_id, $question_id) ?>">
                        <span class="btn btn-default pull-left">
                            <i class="fa fa-long-arrow-left" aria-hidden="true"></i> 
                            Predchádzajúca otázka
                        </span>
                    </a>
                    <a id="next-step" href="<?php echo PollsFrontend::url("next", $poll_id, $question_id) ?>">
                        <span class="btn btn-default pull-right next" id="next_q">
                            Ďalšia otázka<i class="fa fa-long-arrow-right" aria-hidden="true"></i>
                        </span>
                    </a>
                </div>

            </div>

        </div>
    </section>

</div>

<div class="margin-bottom-60"></div>
<script>
    $(document).ready(function () {
        var pollData;
        
        <?php if (Polls::getParam("type", $poll_id) != 3) { ?>
        $("#next-step").click(function() {
            if($("input[name=<?php echo $poll_input_name; ?>]").is(':checked')){
                checked = true;
            }else{
                alert("Prosím vyberte jednu z možností");
                checked = false;
            }
            return checked;
        });  
        <?php } ?>
        
        $("input[name=<?php echo $poll_input_name; ?>]").click(function () {
            if ($("input[name=<?php echo $poll_input_name; ?>]").is(':checked')) {
                pollData = $("input[name=<?php echo $poll_input_name; ?>]:checked").val();
                pollSetCookie("<?php echo $poll_input_name; ?>", pollData, 60);
                console.log(pollData);
            }

            

        });
<?php if (Polls::getParam("type", $poll_id) == 3) { ?>

            $("#next-step").click(function() {
                if($("input[name=<?php echo $poll_input_name; ?>]").val() == ""){
                    alert("Prosím vyplnte textove pole");
                    checked = false;
                }else{
                    checked = true;
                }
                return checked;
               // alert($("input[name=<?php echo $poll_input_name; ?>]").val());
            });  
        
            $("#next-step").click(function () {
                pollData = $("input[name=<?php echo $poll_input_name; ?>").val();
                pollSetCookie("<?php echo $poll_input_name; ?>", pollData, 60);
                console.log(pollData);
            });
<?php } ?>
    });
</script>
<?php get_bottom(); ?>

<?php

function getZobrazenie($stav) {
    foreach (Std::showStatus() as $key => $value) {
        if ($stav == $key)
            echo "<option value='" . $key . "' selected>" . $value . "</option>";
        else
            echo "<option value='" . $key . "'>" . $value . "</option>";
    }
}

function get_top() {
    ?>
    <!DOCTYPE html>
    <html lang="en">
        <head>
            <meta charset="utf-8">
            <!--[if IE]>
            <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
            <![endif]-->
            <meta name="viewport" content="width=device-width, initial-scale=1">
            <meta name="description" content="">
            <meta name="author" content="">
            <title>Admin Polls</title>
            <link rel="icon" href="<?php echo STATIC_FILES_ADMIN ?>img/favicon.ico">

            <!-- BEGIN CSS FRAMEWORK -->
            <link rel="stylesheet" href="<?php echo STATIC_FILES_ADMIN ?>css/bootstrap.min.css">
            <link rel="stylesheet" href="<?php echo STATIC_FILES_ADMIN ?>css/font-awesome.min.css">
            <!-- END CSS FRAMEWORK -->
            <!-- BEGIN CSS PLUGIN -->
            <link rel="stylesheet" href="<?php echo STATIC_FILES_ADMIN ?>css/pace-theme-minimal.css">
            <link rel="stylesheet" href="<?php echo STATIC_FILES_ADMIN ?>css/jquery.gritter.css">

            <link rel="stylesheet" href="<?php echo STATIC_FILES_ADMIN ?>css/jquery-jvectormap-1.2.2.css">
            <link rel="stylesheet" href="<?php echo STATIC_FILES_ADMIN ?>css/blue.css">
            <!-- END CSS PLUGIN -->
            <!-- BEGIN CSS TEMPLATE -->
            <link rel="stylesheet" href="<?php echo STATIC_FILES_ADMIN ?>css/main.css">
            <link rel="stylesheet" href="<?php echo STATIC_FILES_ADMIN ?>css/skins.css">
            <link rel="stylesheet" href="<?php echo STATIC_FILES_ADMIN ?>css/designdnt.css">
            <!-- END CSS TEMPLATE -->
            <!-- CK EDITOR -->
            <!-- this editor is saved in designdnt library as included module -->
            <script src="<?php echo STATIC_FILES_ADMIN ?>../ckeditor/ckeditor.js"></script>
            <!-- BEGIN JS FRAMEWORK -->
            <script src="<?php echo STATIC_FILES_ADMIN ?>js/jquery-2.1.0.min.js"></script>
            <script src="<?php echo STATIC_FILES_ADMIN ?>js/bootstrap.min.js"></script>
            <!-- END JS FRAMEWORK -->
            <!-- BEGIN JS PLUGIN -->
            <script src="<?php echo STATIC_FILES_ADMIN ?>js/pace.min.js"></script>
            <script src="<?php echo STATIC_FILES_ADMIN ?>js/jquery.totemticker.min.js"></script>
            <script src="<?php echo STATIC_FILES_ADMIN ?>js/jquery.ba-resize.min.js"></script>
            <script src="<?php echo STATIC_FILES_ADMIN ?>js/jquery.blockUI.min.js"></script>
            <script src="<?php echo STATIC_FILES_ADMIN ?>js/jquery.gritter.min.js"></script>
            <script src="<?php echo STATIC_FILES_ADMIN ?>js/jquery.sparkline.min.js"></script>
            <script src="<?php echo STATIC_FILES_ADMIN ?>js/icheck.min.js"></script>
            <script src="<?php echo STATIC_FILES_ADMIN ?>summernote.min.js"></script>
            <script src="<?php echo STATIC_FILES_ADMIN ?>js/dnt_custom.js"></script>

            <script src="<?php echo STATIC_FILES_ADMIN ?>js/main_system.js"></script>
            <script src="<?php echo STATIC_FILES_ADMIN ?>js/skin-selector.js"></script>
            <!-- END CK EDITOR -->
        </head>
        <body class="skin-dark">
            <!-- BEGIN HEADER -->
            <header class="header">
                <!-- BEGIN LOGO -->
                <a href="<?php echo STATIC_FILES_ADMIN ?>" class="logo">
                   <!--<img src="<?php echo STATIC_FILES_ADMIN ?>img/logo.png" alt="Kertas" height="20">-->
                    <h3 style="text-align: left; margin-left: 10px;"><span class="designdnt_a">Design</span><span class="designdnt_b">dnt<big><strong>3</strong></big></span></h3>
                </a>
                <!-- END LOGO -->
                <!-- BEGIN NAVBAR -->
                <nav class="navbar navbar-static-top" role="navigation">
                    <a href="#" class="navbar-btn sidebar-toggle" data-toggle="offcanvas" role="button">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="fa fa-bars fa-lg"></span>
                    </a>
                    <!-- BEGIN NEWS TICKER -->
                    <div class="ticker">

                    </div>
                    <!-- END NEWS TICKER -->
                    <div class="navbar-right">

                </nav>
                <!-- END NAVBAR -->
            </header>
            <!-- END HEADER -->
            <div class="wrapper row-offcanvas row-offcanvas-left">
                <!-- BEGIN SIDEBAR -->
                <aside class="left-side sidebar-offcanvas">
                    <section class="sidebar">
                        <div class="user-panel">
                            <div class="pull-left image">
                                <img style="cursor:pointer" onclick="location.href = 'index.php?src=access&action=edit&post_id=20'" src="http://skeleton.localhost/dnt3/dnt-view/data/1/f83920aff7155a9eccf31262549f6d3b.jpg" class="img-circle" alt="Admin Root">
                            </div>
                            <div class="pull-left info">
                                <p><strong style="cursor:pointer" onclick="location.href = 'index.php?src=access&action=edit&post_id=20'" >Admin Root</strong></p>
                                <span ><i class="fa fa-circle text-green"></i> Online</span>
                            </div>
                        </div>
                        <form action="<?php echo STATIC_FILES_ADMIN ?>index.php?src=polls" method="GET" class="sidebar-form">
                            <div class="input-group">
                                <input type="hidden" name="src" value="polls">
                                <input type="text" name="search" class="form-control" placeholder="Hľadať...">
                                <span class="input-group-btn">
                                    <button type="submit" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i></button>
                                </span>
                            </div>
                        </form>
                        <ul class="sidebar-menu">
                            <li class="">
                                <a href="<?php echo WWW_PATH . "admin"; ?>">
                                    <i class="fa fa fa-home"></i>
                                    <span>Domov</span>
                                </a>
                            </li>
                            
                            

                            <li class="menu">
                                <a href="">
                                    <i class="fa fa-user"></i>
                                    <span>Kvízy</span>
                                    <i class="fa fa-angle-left pull-right"></i>
                                </a>
                                <ul class="sub-menu">
                                    <li>
                                        <a href="?action=add_poll">
                                            <span>Pridať kvíz</span>
                                            &nbsp;&nbsp;<i style="text-align: right;" class="fa "></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="<?php echo WWW_PATH; ?>admin/polls">
                                            <span>Zoznam kvízov</span>
                                            &nbsp;&nbsp;<i style="text-align: right;" class="fa "></i>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            
                            <li class="">
                                <a href="<?php echo WWW_PATH . "admin/logout"; ?>">
                                    <i class="fa fa fa-home"></i>
                                    <span>Odhlasit sa</span>
                                </a>
                            </li>

                        </ul>
                    </section>
                </aside>
                <!-- END SIDEBAR -->
                <!-- BEGIN CONTENT -->
                <aside class="right-side">
                    <!-- BEGIN CONTENT HEADER -->
                    <section class="content-header">
                    </section>
                    <section class="content">
                        <div class="row">
                        <?php } ?>
<?php

function get_bottom() { ?>
                        </div>
                    </section>
                    <!-- END CUSTOM TABLE -->
                </aside>
                <!-- END CONTENT -->
                <!-- BEGIN SCROLL TO TOP -->
                <div class="scroll-to-top"></div>
                <!-- END SCROLL TO TOP -->
            </div>
        </body>
    </html>

<?php } ?>
<?php function get_login() { ?>
<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <!--[if IE]>
      <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
      <![endif]-->
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="description" content="">
      <meta name="author" content="">
      <title>Systém | Designdnt</title>
      <link rel="icon" href="<?php echo STATIC_FILES_ADMIN ?>img/favicon.ico">
      <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
      <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
      <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
      <![endif]-->
      <!-- BEGIN CSS FRAMEWORK -->
      <link rel="stylesheet" href="<?php echo STATIC_FILES_ADMIN ?>css/bootstrap.min.css">
      <link rel="stylesheet" href="<?php echo STATIC_FILES_ADMIN ?>css/font-awesome.min.css">
      <!-- END CSS FRAMEWORK -->
      <!-- BEGIN CSS PLUGIN -->
      <link rel="stylesheet" href="<?php echo STATIC_FILES_ADMIN ?>css/pace-theme-minimal.css">
      <link rel="stylesheet" href="<?php echo STATIC_FILES_ADMIN ?>css/jquery.gritter.css">
      <link href="//cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/e8bddc60e73c1ec2475f827be36e1957af72e2ea/build/css/bootstrap-datetimepicker.css" rel="stylesheet">
      <link rel="stylesheet" href="<?php echo STATIC_FILES_ADMIN ?>css/jquery-jvectormap-1.2.2.css">
      <link rel="stylesheet" href="<?php echo STATIC_FILES_ADMIN ?>css/blue.css">
      <!-- END CSS PLUGIN -->
      <!-- BEGIN CSS TEMPLATE -->
      <link rel="stylesheet" href="<?php echo STATIC_FILES_ADMIN ?>css/main.css">
      <link rel="stylesheet" href="<?php echo STATIC_FILES_ADMIN ?>css/skins.css">
      <link rel="stylesheet" href="<?php echo STATIC_FILES_ADMIN ?>css/designdnt.css">
      <!-- END CSS TEMPLATE -->
      <!-- CK EDITOR -->
      <!-- this editor is saved in designdnt library as included module -->
      <script src="../dnt-library/ckeditor/ckeditor.js"></script>
	  
	   <!-- BEGIN JS FRAMEWORK -->
      <script src="<?php echo STATIC_FILES_ADMIN ?>js/jquery-2.1.0.min.js"></script>
      <script src="<?php echo STATIC_FILES_ADMIN ?>js/bootstrap.min.js"></script>
      <!-- END JS FRAMEWORK -->
      <!-- BEGIN JS PLUGIN -->
	  <script src="<?php echo STATIC_FILES_ADMIN ?>js/pace.min.js"></script>
      <script src="<?php echo STATIC_FILES_ADMIN ?>js/jquery.totemticker.min.js"></script>
      <script src="<?php echo STATIC_FILES_ADMIN ?>js/jquery.ba-resize.min.js"></script>
      <script src="<?php echo STATIC_FILES_ADMIN ?>js/jquery.blockUI.min.js"></script>
      <script src="<?php echo STATIC_FILES_ADMIN ?>js/jquery.gritter.min.js"></script>
      <script src="<?php echo STATIC_FILES_ADMIN ?>js/jquery.sparkline.min.js"></script>
      <script src="<?php echo STATIC_FILES_ADMIN ?>js/icheck.min.js"></script>
      <script src="<?php echo STATIC_FILES_ADMIN ?>summernote.min.js"></script>
      <script src="<?php echo STATIC_FILES_ADMIN ?>js/dnt_custom.js"></script>
      <!--[if lte IE 8]>
      <script language="javascript" type="text/javascript" src="<?php echo STATIC_FILES_ADMIN ?>js/excanvas.min.js"></script>
      <![endif]-->
      <!-- END JS PLUGIN -->
      <!-- BEGIN JS TEMPLATE -->
      <script src="<?php echo STATIC_FILES_ADMIN ?>js/main_system.js"></script>
      <script src="<?php echo STATIC_FILES_ADMIN ?>js/skin-selector.js"></script>
	  
	  <script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment-with-locales.js"></script>
  <script src="//cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/e8bddc60e73c1ec2475f827be36e1957af72e2ea/src/js/bootstrap-datetimepicker.js"></script>
	
      <!-- END CK EDITOR -->
   </head>
<body class="login">
<div class="outer">
	<div class="middle">
		<div class="inner">
			<div class="row">
				<!-- BEGIN LOGIN BOX -->
				<div class="col-lg-12">
					<h3 class="text-center login-title">Prihláste sa pre pokračovanie</h3>
					<div class="account-wall">
						<!-- BEGIN PROFILE IMAGE -->
						<img class="profile-img" src="<?php echo STATIC_FILES_ADMIN ?>img/designdnt_singl_dark.png" alt="">
						<!-- END PROFILE IMAGE -->
						<!-- BEGIN LOGIN FORM -->
						<form name="login" action="<?php echo WWW_PATH."admin/login/autorize" ?>" method="POST" class="form-login">
							<input type="text" name="email" class="form-control" placeholder="Email, alebo login" autofocus>
							
							<input type="password" name="pass" class="form-control" placeholder="Heslo">
							<button class="btn btn-lg btn-primary btn-block" name="sent" type="submit">Prihlásiť sa</button>
							<label class="checkbox pull-left">
								<!--<input type="checkbox" value="remember-me">Remember me-->
							</label>
							<a href="http://designdnt.query.sk/" target="_blank" class="pull-right need-help">Potrebujete pomôcť?</a><span class="clearfix"></span>
						</form>
						<!-- END LOGIN FORM -->
					</div>
					<a href="<?php echo STATIC_FILES_ADMIN ?>index.php?src=login" class="text-center new-account">Zabudol som heslo</a>
				</div>
				<!-- END LOGIN BOX -->
			</div>
		</div>
	</div>
</div>

     
   </body>
</html>
    
<?php } ?>

